#!/bin/bash

convertingfile=$1
outputfile=$2

timeinsec = ffprobe -v error -show_entries format=duration \
  -of default=noprint_wrappers=1:nokey=1 $convertingfile
echo "$timeinsec"

mkdir frames
ffmpeg -i $convertingfile -t 5 -r 10 ./frames/output%03d.png
convert -delay 10 -loop 0 ./frames/output*.png $outputfile 
