FROM ubuntu:16.04
RUN apt-get update && apt-get install -y \
    imagemagick \
    python3-pip
RUN pip3 install --upgrade pip
WORKDIR /gifmaker
ADD requirements.txt .
RUN pip3 install -r requirements.txt
COPY . /gifmaker
COPY make_thumbnail /bin
