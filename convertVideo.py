import subprocess
import sys

args = sys.argv
# print(str(sys.argv))
# print(len(sys.argv))
videoduration = float((subprocess.check_output(['ffprobe','-v','error','-show_entries','format=duration','-of','default=noprint_wrappers=1:nokey=1',args[1]])).strip('\n'))
startstamp = int((videoduration/3) * 2)
print('videoduration(in seconds) : ',videoduration)
print('startstamp(in int) : ',startstamp)
remainingafterstart = int(videoduration-startstamp)
if remainingafterstart<10:
    subprocess.check_output(['ffmpeg','-i',args[1],'-t',str(remainingafterstart),'-r','10','./frames/output%03d.png'])
else:
    subprocess.check_output(['ffmpeg','-i',args[1],'-t','10','-r','10','./frames/output%03d.png'])
subprocess.check_output(['convert','-delay','10','-loop','0','./frames/output*.png',args[2]])
